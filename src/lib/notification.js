var admin = require('firebase-admin');
var Logger = require('../helper/logger');

var logger = new Logger().getInstance();

function init() {
    logger.info('initialize firebase sdk')
    admin.initializeApp({
        credential: admin.credential.applicationDefault(),
    });
}

async function sendToDevices(tokens, message) {
    var fcm = {
        data: message,
        token: tokens
    }

    return admin.messaging().send(fcm);
}

async function sendToTopic(topic, message) {
    var fcm = {
        data: message,
        topic: topic
    }

    logger.info(JSON.stringify(fcm.data));

    return admin.messaging().send(fcm);
}

async function subscribeToken(tokens, topic) {
    logger.info("subscribed " + tokens + "to topic " + topic);
    return admin.messaging().subscribeToTopic(tokens, topic)
}

module.exports = {
    init,
    sendToDevices,
    sendToTopic,
    subscribeToken
}