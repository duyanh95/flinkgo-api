var winston = require('winston');
var { format, transports } = winston;


module.exports = {
    dev: {
        level: 'info',
        format: format.simple(),
        transports: [
            new winston.transports.Console()
        ]
    },
    production: {
        level: 'info',
        format: format.combine(
            format.timestamp('unix'),
            format.json()
        ),
        transports: [
            new transports.Console(),
            new transports.File({ filename: '/tmp/flinkgo.log' })
        ]
    },
    debug: {
        level: 'debug',
        format: format.simple(),
        transports: [
            new transports.Console()
        ]
    }
}