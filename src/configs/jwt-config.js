require('dotenv').config();
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));

var config = {
    ALG: 'RS256',
    EXPIRE: process.env.TOKEN_EXP
}
var publicKey = null;
var privateKey = null;

function getConfig() {
    if (!publicKey || !privateKey) {
        Promise.all([fs.readFileAsync(process.env.PRIVATE_KEY), 
            fs.readFileAsync(process.env.PUBLIC_KEY)])
        .then(keys => {
            config.PUBLIC_KEY = keys[1];
            config.PRIVATE_KEY = keys[0];
        }).catch(err => {
            console.log(err)
            process.exit(1)
        })

        return config;
    }

}

module.exports = getConfig();