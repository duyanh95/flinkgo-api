var {Schema} = require('mongoose');

module.exports = new Schema({
    detailAddress: String,
    coordinates: [Number]
}, { _id: false });
