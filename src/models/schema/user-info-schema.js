var { Schema } = require('mongoose');
var addressSchema = require('./address-schema');

module.exports = new Schema({
    id: {
        type: Schema.Types.ObjectId,
    },

    username: {
        type: String,
        required: true
    },

    firstName: String, 
    
    lastName: String,

    dob: Date,
    
    address: addressSchema,

    avatar: String, 

    phoneNumber: String,
}, { _id: false });