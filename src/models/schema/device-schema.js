var {Schema} = require('mongoose');

module.exports = new Schema({
    id: String,
    token: String
}, { _id: false });
