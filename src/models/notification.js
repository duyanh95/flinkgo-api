var mongoose = require('mongoose');

var dataSchema = new mongoose.Schema({}, { strict: false, _id: false })

var notificationSchema = new mongoose.Schema({
    type: {
        type: String,
        required: true
    },

    data: {
        type: dataSchema,
        required: true
    },

    topic: String,

    sentAt: {
        type: Date,
        required: true
    }
});

var NotificationModel = mongoose.model('Notification', notificationSchema);

module.exports = NotificationModel