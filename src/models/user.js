var mongoose = require('mongoose');
var addressSchema = require('./schema/address-schema');
var deviceSchema = require('./schema/device-schema');

var authenticationSchema = new mongoose.Schema({
    password: {
        type: String,
        required: true
    },

    salt: {
        type: String,
        required: true
    },
    
    refreshToken: String,

    status: Number
});

var userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    
    firstName: String, 
    
    lastName: String,

    authentication: {
        type: authenticationSchema,
        required: true
    },

    dob: Date,
    
    address: addressSchema,

    avatar: String, 

    nativeLanguage: [Number],

    targetLanguage: [Number],

    phoneNumber: String,

    schedule: [Number],

    device: [deviceSchema],

    lastOnline: Date
});

var userModel = mongoose.model('User', userSchema);

module.exports = userModel;