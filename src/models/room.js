var mongoose = require('mongoose');
var userInfoSchema = require('./schema/user-info-schema');

var roomSchema = new mongoose.Schema({
    users: [userInfoSchema],

    name: String,

    category: {
        type: [Number],
        required: true
    },

    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },

    createdAt: {
        type: Date,
        required: true
    },

    visible: Boolean,

    startVisibleAt: Date
});

var RoomModel = mongoose.model('Room', roomSchema);

var addUser = function(user) {
    if (! user instanceof mongoose.model)
        throw new Error('Populate with invalid object');

    var userInfo = {
        id: user._id,
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        dob: user.dob,
        address: user.address,
        avatar: user.avatar,
        phoneNumber: user.phoneNumber
    };

    var existed = this.users.some((currUser, idx) => {
        let sameId = (currUser.id.equals(user._id));
        
        this.users[idx] = sameId ? userInfo : currUser;
        return sameId;
    });


    if (!existed) {
        this.users.push(userInfo);
    }
};

module.exports = {
    RoomModel,
    addUser
}
