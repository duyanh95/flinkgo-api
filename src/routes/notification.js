var router = require('express').Router();
const authorizeService = require('../services/authorization-service');
const pageValidate = require('../validators/pagination-validator');
const notiService = require('../services/notification-service');
const moment = require('moment-timezone');

router.get('/', authorizeService.authorizeRequest, async (req, res, next) => {
    try {
        var pagination = await pageValidate.validatePagination(req.query);

        if (!pagination.page)
            pagination.page = 0;
        if (!pagination.size)
            pagination.size = 10;

        var notis = await notiService.getUserNotification(req.locals.user, pagination)
        var resp = notis.map(convertNotificationToResponseData)

        res.send({ notification: resp });
    } catch (e) {
        next(e);
    }
});

function convertNotificationToResponseData(noti) {
    var newNoti = noti.toObject();

    delete newNoti.__v;

    newNoti.id = noti._id;
    delete newNoti._id;

    newNoti.sentAt = noti.sentAt.getTime();

    return newNoti;
}

module.exports = router;