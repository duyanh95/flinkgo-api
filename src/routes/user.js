var router = require('express').Router();
var authorizeService = require('../services/authorization-service');
var scheduleValidator = require('../validators/schedule-validator');
var userValidator = require('../validators/user-validator');
var userService = require('../services/user-info-service');
var UserResponse = require('../dto/user-response');

router.patch('/', authorizeService.authorizeRequest, function (req, res, next) {
    userValidator.updateUserInfoRequestValidator(req.body)
    .then(rs => userService.updateInfo(req.locals.user, req.body))
    .then(rs => res.send({result: 'sucess'}))
    .catch(next);
})

router.post('/schedule', authorizeService.authorizeRequest, function (req, res, next) {
    scheduleValidator.validateScheduleRequest(req.body)
    .then(rs => userService.createSchedule(req.locals.user, req.body))
    .then(rs => res.send({result: 'sucess'}))
    .catch(next);
})

router.get('/', authorizeService.authorizeRequest, function (req, res, next) {
    userService.getUser(req.locals.user)
    .then(user => res.send({ user: new UserResponse(user) }))
    .catch(next);
})

module.exports = router;