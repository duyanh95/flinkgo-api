const router = require('express').Router();
const encrypt = require('../helper/encryption');
var UserModel = require('../models/user');
var { RoomModel, addUser } = require('../models/room');
var UserModel = require('../models/user');
var Logger = require('../helper/logger')
var notificationService = require('../services/notification-service');
const InvitationNotification = require('../dto/invite-notification-data');
var authorizeService = require('../services/authorization-service');

var logger = new Logger().getInstance();

router.get('/', function(req, res, next) {
    logger.info('log'); 
});

router.get('/test-firebase', async function(req, res, next) {
    notification.subscribeToken("abcd", "system").catch(next);
});

// this is just dummy code for development purpose
router.post('/notification', authorizeService.authorizeRequest, async function(req, res, next) {
    var room = {
        _id: "ii6Thogh2Aezooz0",
        name: "test_room",
        category: [1]
    }

    UserModel.findOne({ username: req.query.username }).exec()
    .then(
        user => notificationService.sendNotificationToUser(new InvitationNotification(req.locals.user, user, room), user)
    ).then(rs => res.send({ result: 'ok'})
    ).catch(next);
});

module.exports = router;