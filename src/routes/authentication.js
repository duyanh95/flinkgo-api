var router = require('express').Router();
var { RequestError } = require('../custom-error');
var authValidator = require('../validators/auth-validator');
var registerValidator = require('../validators/user-validator');
var authService = require('../services/authentication-service');
var authorizeService = require('../services/authorization-service');
var regService = require('../services/user-register-service');
var UserResponse = require('../dto/user-response');
var notiService = require('../services/notification-service')


const TOKEN_HEADER = 'Device-Token';
const DEVICE_HEADER = 'Device-Id';

// login request
router.post('/login', function loginUser(req, res, next) {
    authValidator.loginRequestValidate(req.body)
    .then(authService.authenticateLoginUser)
    .then(user => {
        sendUserResp(user, res);
        saveDevice(req, user);
    })
    .catch(next);
});

// refresh token
router.post('/refresh', function refreshUserToken(req, res, next) {
    var header = req.header('Authorization');

    if (!header)
        throw new AuthenticationError('Refresh token invalid');

    var headerParts = header.split(' ');
    if (headerParts[0] !== 'Refresh') 
        throw new AuthenticationError('Request not authenticated');

    var token = headerParts[1];
    
    authValidator.refreshTokenValidate(req.body)
    .then(
        body => authService.refreshUserWithToken(token, body)
    )
    .then(user => {
        sendUserResp(user, res);
        saveDevice(req, user);
    })
    .catch(next);
});

// user register
router.post('/register', function registerUserRequest(req, res, next) {
    registerValidator.registerRequestValidate(req.body)
    .then(regService.registerUserAndGenerateTokens)
    .then(user => {
        sendUserResp(user, res);
        saveDevice(req, user);
    })
    .catch(next);
});

// send user repsonse
function sendUserResp(user, res) {
    res.send({
        user: new UserResponse(user),
        accessToken: user.authentication.accessToken,
        refreshToken: user.authentication.refreshToken
    });

    return user;
}

function saveDevice(req, user) {
    let device = {
        id: req.header(DEVICE_HEADER),
        token: req.header(TOKEN_HEADER)
    }

    if (device.id && device.token) {
        return notiService.saveUserDevice(user, device);
    }
}

router.get('/service-test', authorizeService.authorizeRequest);

module.exports = router;