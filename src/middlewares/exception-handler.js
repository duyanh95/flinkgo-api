const { RequestError, AuthenticationError, AuthorizationError, DataConflictError } 
    = require('../custom-error');
var Logger = require('../helper/logger');

var logger = new Logger().getInstance();

function handleException(err, req, res, next) {
    logger.warn(err);
    logger.debug(err.stack);
    respErr = {
        status: 500,
        error: err.name,
        message: err.message
    }

    if (err instanceof DataConflictError) {
        respErr.status = 409;
    } else if (err instanceof RequestError) {
        respErr.status = 400;
    } else if (err instanceof AuthenticationError) {
        respErr.status = 401;
    } else if (err instanceof AuthorizationError) {
        respErr.status = 403;
    } else if (req.xhr) {
        respErr.error = 'Internal server error';
    } else {
        respErr.error = 'Internal server error';
    }

    res.status(respErr.status).send(respErr);
}

module.exports = handleException;