var bodyParser = require('body-parser');
var { RequestError } = require('../custom-error');

function jsonParse(req, res, next) {
    bodyParser.json()(req, res, (err) => {
        if (err) 
            return next(new RequestError(err.message));
        else
            return next();
    })
}

module.exports = jsonParse;