const coordinate = require('../helper/coordinate-helper');
const Logger = require('../helper/logger');

const log = new Logger().getInstance();

class InviteNotificationData {
    constructor(owner, user, room) {
        var noti = {};
        log.info(JSON.stringify(owner));

        noti = {
            inviteTime: new Date().getTime().toString(),
            inviteUser: JSON.stringify({
                id: owner.id,
                username: owner.username,
                avatar: owner.avatar
            }),
            group: JSON.stringify({
                id: room._id,
                name: room.name,
                category: room.category
            }),
            message: "bạn đã được " + owner.username + " mời vào nhóm " + room.name
        };

        try {
            noti.data.distance = coordinate.calculateDistance(owner.address.coordinates, user.address.coordinates)
        } catch (e) {
            log.warn(e)
        }



        // noti.type = "INVITATION";

        return noti;
    }
}

module.exports = InviteNotificationData;

