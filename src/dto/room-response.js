const moment = require('moment-timezone');

class RoomResponse {
    constructor(room) {
        console.log(room)
        try {
            var resp = room.toObject();
        } catch(e) {
            resp = room;
        }
        
        delete resp._id;
        delete resp.__v;

        resp.id = room._id;
        
        return resp;
    }
}

module.exports = RoomResponse;