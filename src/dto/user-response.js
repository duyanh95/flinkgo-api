const moment = require('moment-timezone');
const converter = require('../services/schedule-converter');

class UserResponse {
    constructor(user) {
        console.log(user)
        try {
            var resp = user.toObject();
        } catch(e) {
            resp = user;
        }
        
        delete resp.authentication;
        delete resp._id;
        delete resp.__v;
        delete resp.device;

        if (resp.schedule && resp.schedule.length)
            resp.schedule = converter.convertScheduleDataToResponse(resp.schedule);

        resp.id = user._id;
        resp.dob = moment(user.dob).tz('UTC').format('YYYY-MM-DD');
        
        if (resp.address && resp.address.coordinates) {
            resp.address.longitude = user.address.coordinates[0];
            resp.address.latitude = user.address.coordinates[1];
            delete resp.address.coordinates;
        }

        return resp;
    }
}

module.exports = UserResponse;