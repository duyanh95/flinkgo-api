'use strict';

require('dotenv').config();
const express = require('express');
const jsonParser = require('./middlewares/json-parser');
const exceptionHandler = require('./middlewares/exception-handler');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var Logger = require('./helper/logger');
const notification = require('./lib/notification');

const logger = new Logger().getInstance();

const app = express();
mongoose.connect(process.env.MONGO, {useNewUrlParser: true});

notification.init();

var db = mongoose.connection;

mongoose.Promise = Promise;
db.on('error', err => {
    logger.error(err);
    process.exit(1);
});

app.use(jsonParser);

app.use('/auth', require('./routes/authentication'));
app.use('/user', require('./routes/user.js'));
app.use('/test', require('./routes/test'));
app.use('/notification', require('./routes/notification'));

app.use(exceptionHandler);

app.listen(process.env.PORT || 8080);