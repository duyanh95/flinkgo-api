
function arrayHasDuplicates(arr) {
    return arr.some( item => {
        return arr.indexOf(item) !== arr.lastIndexOf(item);
    });
}

function arrayHasDuplicateProperties(arr, prop) {
    return arr.some((it, idx) => {
        var duplicate = false;
        var max_idx = arr.length - 1;

        arr.slice().reverse().every(( rit, ridx) => {
            if (rit[prop] === it[prop] && ridx !== (max_idx - idx)) {
                duplicate = true;
                return false;

            } else {
                return true;
            }

        })

        return duplicate;
    });
}

module.exports = {
    arrayHasDuplicates,
    arrayHasDuplicateProperties
}