var Promise = require('bluebird');
var jwt = Promise.promisifyAll(require('jsonwebtoken'));
var exception = require('../custom-error');
var config = require('../configs/jwt-config');
var { AuthenticationError } = require('../custom-error');

var verify = Promise.promisify(jwt.verify);

async function generateJWT(user) {
    authenticateUser = {
        id : user.id,
        username: user.username,
        fullname: user.fullname,
        avatar: user.avatar
    }

    return jwt.sign({ user: authenticateUser }, config.PRIVATE_KEY, { algorithm: config.ALG, expiresIn: config.EXPIRE });
}

async function verifyJWT(token) {
    return verify(token, config.PUBLIC_KEY)
    .catch(err => { throw new AuthenticationError(err.message) });
}

module.exports = {
    generateJWT,
    verifyJWT
}