require('../helper/date-helper');


function createOnlineQuery(isOnline, currentTime) {
    if (isOnline)
        return {"lastOnline": {$gte: {new Date(currentTime - process.env.ONLINE_DURATION)}}};
    else
        return {"lastOnline": {$lt: {new Date(currentTime - process.env.ONLINE_DURATION)}}};
}

function createAgeDifferenceQuery(boundary, dob) {
    var lowerBound = boundary[0], upperBound = boundary[1];
    if (lowerBound !== 0 && upperBound == null) {
        return {$or: [{"age": {$gte: dob.addYear(lowerBound)}, {"age": {$lte: dob.addYear(-lowerBound)}}]}
    } else if (lowerBound === 0 && upperBound != null) {
        return {"age": {$lt: dob.addYear(upperBound), $gt:dob.addYear(-upperBound)}};
    } else if (upperBound != null && upperBound > lowerBound) {
        youngerRange = {$lte: dob.addYear(-lowerBound), $gt: dob.addYear(-upperBound)};
        olderRange = {$gte: dob.addYear(lowerBound), $lt: dob.addYear(upperBound)};

        return {$or: [{"age": youngerRange}, "age": olderRange]};    
    } else {
        throw new Error("age criteria range is invalid")
    }
}

function createLocationQuery(boundary, location)  {
    var lowerBound = boundary[0], upperBound = boundary[1];

    if (lowerBound !== 0 && upperBound == null) {
        return createLocationQuery(location, upperBound, lowerBound)
    } else if (lowerBound === 0 && upperBound != null) {
        return createLocationQuery(location, null, upperBound);
    } else if (upperBound != null && upperBound > lowerBound) {
        return createLocationQuery(location, lowerBound, upperBound);
    } else {
        throw new Error("location criteria is invalid");
    }
}

function createLocatiuonQueryOnDistance(location, minDistance, maxDistance) {
    var nearQuery = {
        $geometry: {
            type: "Point",
            coordinates: location
        }
    }

    if (minDistance)
        nearQuery.$minDistance = minDistance;

    if (maxDistance)
        nearQuery.$maxDistance = maxDistance;

    return {"address.coordinates": {
        $near: nearQuery
    }};
}

module.exports = {
    createOnlineQuery,
    createAgeDifferenceQuery,
    createLocationQuery
}
