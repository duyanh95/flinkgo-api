var Promise = require('bluebird');
const crypto = require('crypto');

function pbkdf2Async(password, salt, iterations, keylen, digest) {
    return new Promise((res, rej) => {
        crypto.pbkdf2(password, salt, iterations, keylen, digest, (err, key) => {
            err ? rej(err) : res(key);
        });
    });
}

var randomBytes = Promise.promisify(crypto.randomBytes);

const CONFIG = {
    iterations: 100000,
    bytes: 64,
    algo: 'sha512'
}

function genrateRandomBytes(length) {
    return randomBytes(length).then(data => data.toString('hex'));
}

function passwordEncrypt(password, salt) {
    return pbkdf2Async(password, salt, CONFIG.iterations, CONFIG.bytes, CONFIG.algo)
        .then(data => data.toString('hex'));
}

function passwordVerify(password, salt, encoded) {
    return pbkdf2Async(password, salt, CONFIG.iterations, CONFIG.bytes, CONFIG.algo)
    .then(data => {
        if (data.toString('hex') !== encoded) {
            throw new Error('password invalid');
        }

        return data;
    });
}

module.exports = {
    genrateRandomBytes,
    passwordEncrypt,
    passwordVerify
}