var winston = require('winston');
require('dotenv').config();

const options = require('../configs/winston-logging-config');


class Logger
{
    constructor() {
        if (!Logger.instance) {
            Logger.instance = winston.createLogger(options[process.env.ENV]);
            Logger.instance.info(process.env.ENV);
        }
    }

    getInstance() {
        return Logger.instance;
    }
}

module.exports = Logger;