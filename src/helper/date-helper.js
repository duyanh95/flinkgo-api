Date.prototype.addYear = function(year) {
    this.setFullYear(this.getFullYear() + year);
}