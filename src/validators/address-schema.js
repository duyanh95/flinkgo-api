const {constraint} = require('./validator');

const addressSchema = {
    detailAddress: constraint.string(),
    latitude: constraint.number().required(),
    longitude: constraint.number().required()
};

module.exports = addressSchema;