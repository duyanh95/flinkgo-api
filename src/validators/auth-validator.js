const {validate, constraint} = require('./validator');
const { RequestError } = require('../custom-error');
const addressSchema = require('./address-schema');

function loginRequestValidate(data) {
    const schema = {
        username: constraint.string().min(4).required(),
        password: constraint.string().min(6).required(),
        address: addressSchema
    };

    return validate(data, schema).then(
        val => {
            if (val.address)
                val.address.coordinates = [val.address.longitude, val.address.latitude];
            return val;
        }
    );
};

function refreshTokenValidate(data) {
    const schema = {
        address: addressSchema
    }

    return validate(data, schema).then(
        val => {
            if (val.address)
                val.address.coordinates = [val.address.longitude, val.address.latitude];
            return val;
        }
    );
}

module.exports = {
    loginRequestValidate,
    refreshTokenValidate
};
