const {validate, constraint} = require('./validator');
const { RequestError } = require('../custom-error');

function validatePagination(data) {
    var schema = {
        page: constraint.number().min(0),
        size: constraint.number().min(1).max(200)
    }

    return validate(data, schema);
}

module.exports = {
    validatePagination
};