const BaseJoi = require('@hapi/joi');
const Extension = require('@hapi/joi-date');
var { RequestError } = require('../custom-error');

function validate(data, schema, callback) {
    return new Promise(function(resolve, reject) {
        BaseJoi.validate(data, schema, function (err, res) {
            if (err)
                return reject(new RequestError(err.details[0].message));

            return resolve(data);
        })
    });
}


const constraint = BaseJoi.extend(Extension);

module.exports = {
    validate,
    constraint
};