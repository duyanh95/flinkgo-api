const {validate, constraint} = require('./validator');
const { RequestError } = require('../custom-error');
const helper = require('../helper/duplication-helper');

const daySchema = constraint.object({
    day: constraint.number().min(0).max(6).required(),
    time: constraint.array().items(constraint.number().min(0).max(3)).min(1).max(4).required()
});

const scheduleSchema = {
    schedule: constraint.array().items(daySchema).required()
}



function timeInDaysValidate(data) {
    data.schedule.forEach(day => {
        if (helper.arrayHasDuplicates(day.time))
            throw new RequestError('"time" must not be duplicated')
    })

    return data;
}

function dayInScheduleValidate(data) {
    if (helper.arrayHasDuplicateProperties(data.schedule, 'day'))
        throw new RequestError('"day" must not be duplicated');

    return data;
}

function validateScheduleRequest(data) {
    return validate(data, scheduleSchema)
    .then(dayInScheduleValidate)
    .then(timeInDaysValidate);
}

module.exports = {
    validateScheduleRequest
}