const {validate, constraint} = require('./validator');
const { RequestError } = require('../custom-error');
const duplicationHelper = require('../helper/duplication-helper')

function createRoomRequestValidate(data) {
    const schema = {
        name: constraint.string().required(),
        userIds: constraint.array().items(constraint.string()).min(1).required(),
        category: constraint.array().items(constraint.number().integer()).min(1).required()
    }

    return validate(data, schema)
    .then(data => {
        if (duplicationHelper.arrayHasDuplicates(data.userIds)) {
            throw new RequestError('"userIds" must not be duplicated')
        }

        if (duplicationHelper.arrayHasDuplicates(data.category)) {
            throw new RequestError('"category" must not be duplicated')
        }

        return data;
    })
}

module.exports = {
    createRoomRequestValidate
}