const {validate, constraint} = require('./validator');
const { RequestError } = require('../custom-error');
const moment = require('moment-timezone');
const helper = require('../helper/duplication-helper');
const addressSchema = require('./address-schema');


const registerSchema = {
    username: constraint.string().min(4).required(),
    password: constraint.string().min(8).required(),
    firstName: constraint.string().required(),
    lastName: constraint.string().required(),
    avatar: constraint.string(),
    dob: constraint.date().format('YYYY-MM-DD').required(),
    gender: constraint.number().min(0).max(2).required(),
    phoneNumber: constraint.string().regex(/[0-9]+/).min(8).max(11).required(),
    address: addressSchema
};

const updateInfoSchema = {
    firstName: constraint.string(),
    lastName: constraint.string(),
    avatar: constraint.string(),
    dob: constraint.date().format('YYYY-MM-DD'),
    gender: constraint.number().min(1).max(2),
    phoneNumber: constraint.string().regex(/[0-9]+/).min(8).max(11),
    nativeLanguage: constraint.array().items(constraint.number().min(0)),
    targetLanguage: constraint.array().items(constraint.number().min(0)),
    address: addressSchema
}

function registerRequestValidate(data) {
    return validate(data, registerSchema)
    .then(val => {
        val.dob = moment.tz(val.dob, 'UTC');

        if (val.address)
            val.address.coordinates = [val.address.longitude, val.address.latitude];
        
        return val;
    });
};

function checkLanguageNotDuplicate(data) {
    if (data.nativeLanguage && helper.arrayHasDuplicates(data.nativeLanguage) )
        throw new RequestError('"nativeLanguage" must not be duplicated');

    if (data.targetLanguage && helper.arrayHasDuplicates(data.targetLanguage) )
        throw new RequestError('"targetLanguage" must not be duplicated');


    return data;
}

function updateUserInfoRequestValidator(data) {
    return validate(data, updateInfoSchema)
    .then(checkLanguageNotDuplicate)
    .then(val => {
        val.dob = moment.tz(val.dob, 'UTC');

        if (val.address)
            val.address.coordinates = [val.address.longitude, val.address.latitude];

        return val;
    });
}

module.exports = {
    registerRequestValidate,
    updateUserInfoRequestValidator
};