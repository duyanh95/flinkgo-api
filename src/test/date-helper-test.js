require('../helper/date-helper');
var assert = require('assert');
var expect = require('chai').expect;

describe('add year', function() {
    it('should get correct date', function() {
        var date = new Date("2010-01-01");
        date.addYear(1)
        expect(date.getFullYear()).equal(2011);

        date.addYear(-10)
        expect(date.getFullYear()).equal(2001);
    })
})
    
