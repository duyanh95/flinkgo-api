var validator = require('../validators/room-validator');
var assert = require('assert');
var expect = require('chai').expect;
const { RequestError } = require('../custom-error');

validTestCases = [
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu'],
        category: [0]
    },
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu', 'eafaegfvcvrgfgafr'],
        category: [0]
    },
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu'],
        category: [0, 1]
    },
]

invalidTestCases = [
    {
        userIds: ['cdajnihguitgyudtcu', 'eafaegfvcvrgfgafr'],
        category: [0, 1]
    },
    {
        name: 'test room',
        userIds: [],
        category: [0, 1]
    },
    {
        name: 'test room',
        category: [0, 1]
    },
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu'],
        category: []
    },
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu'],
    },
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu'],
        category: [0.3, 1]
    },
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu', 'cdajnihguitgyudtcu'],
        category: [0]
    },
    {
        name: 'test room',
        userIds: ['cdajnihguitgyudtcu'],
        category: [1, 1]
    },
];

describe('test create room request validate', function() {
    invalidTestCases.forEach((_case, idx) => {
        it('case ' + idx + ' should be invalid', async function() {
            await expect(validator.createRoomRequestValidate(_case)).to.eventually.rejectedWith(RequestError);
        })
    })

    validTestCases.forEach((_case, idx) => {
        it('case ' + idx + ' should be valid', async function() {
            const val = await validator.createRoomRequestValidate(_case)
            expect(val).equal(_case);
        })
    })
})