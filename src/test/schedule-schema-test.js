var validator = require('../validators/schedule-validator');
var assert = require('assert');
require('dotenv').config();

const validCases = [
    { schedule: [{ day: 1, time: [0, 1] }, { day: 0, time: [0] }]},
    { schedule: [{ day: 1, time: [0, 1] }]},
    { schedule: [] },
    { schedule: [
        { day: 0, time: [0, 1, 2, 3]}, { day: 1, time: [0, 1, 2, 3]},
        { day: 2, time: [0, 1, 2, 3]}, { day: 3, time: [0, 1, 2, 3]},
        { day: 4, time: [0, 1, 2, 3]}, { day: 5, time: [0, 1, 2, 3]},
        { day: 6, time: [0, 1, 2, 3]}
    ]}
];

const invalidCases = [
    { schedule: [{ day: 0 }]},
    { schedule: [{ time: [0] }]},
    { schedule: [{ day: 1, time: [0, 1] }, { day: 1, time: [0] }]},
    { schedule: [{ day: 0, time: [4] }]},
    { schedule: [{ day: 7, time: [0] }]},
    { schedule: [{ day: 6, time: [0, 4] }]},
    { schedule: [{ day: 6, time: [1, 1] }]},
    { schedule: [{ day: 6, time: [0, 1, 2, 2] }]},
    { schedule: [{ day: 1, time: [0, 1] }, { day: 1, time: [0] }]},
    { schedule: [{}] }
];

describe('Data check: ', function() {
    validCases.forEach(value => {
        it(JSON.stringify(value) + ' should be valid', async function() {
            await validator.validateScheduleRequest(value);

        });
    });


    invalidCases.forEach(value => {
        it(JSON.stringify(value) + ' should be invalid', async function() {
            await assert.rejects(async () => await validator.validateScheduleRequest(value),
                Error);
        });
    });
});
