var converter = require('../services/schedule-converter');
var assert = require('assert');
var expect = require('chai').expect;

const testCases = [
    {
        data: 0x00000000,
        schedule: []
    },
    {
        data: 0x00000001,
        schedule: [
            { day: 0, time: [0] }
        ]
    },
    {
        data: 0x00300011,
        schedule: [
            { day: 0, time: [0] },
            { day: 1, time: [0] },
            { day: 5, time: [0, 1]}
        ]
    },
    {
        data: 0x0f0070f1,
        schedule: [
            { day: 0, time: [0] },
            { day: 1, time: [0, 1, 2, 3] },
            { day: 3, time: [0, 1, 2] },
            { day: 6, time: [0, 1, 2, 3]}
        ]
    }
]

describe('test schedule conversion', function() {
    testCases.forEach(caseValue => {
        it('data: ' + caseValue.data.toString(16) + ' should be the converted result', function() {
            expect(converter.convertRequestToScheduleData(caseValue.schedule)).equal(caseValue.data);
        });

        it('data: ' + caseValue.data.toString(16) + ' conversion should be correct', function() {
            expect(converter.convertScheduleDataToResponse(caseValue.data).sort()).deep.equal(caseValue.schedule.sort());
        });
    })
})

const schedule = [
    
]