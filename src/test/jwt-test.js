var jwt = require('../helper/jwt-authentication');
var assert = require('assert');
require('dotenv').config();

describe('JWT generation', function() {
    it('should be able to encoded', function(done) {
        jwt.generateJWT({ username: 'user' }, 10000)
        .then(jwt.verifyJWT)
        .then(decoded => {
            console.log(decoded);

            try {
                assert.equal(decoded.user.username, 'user');
            } catch(err) {
                return done(err);
            }

            done();
        });
    })
});
