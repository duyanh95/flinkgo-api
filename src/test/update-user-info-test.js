var validators = require('../validators/user-validator');
var assert = require('assert');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

var expect = chai.expect;
const { RequestError } = require('../custom-error');

const validTestCase = [
    {
        firstName: 'test',
        lastName: 'test',
        dob: '1993-02-24',
        gender: 1,
        phoneNumber: '0123456789',
        address: {
            detailAddress: 'test address',
            latitude: 22.32424,
            longitude: 134.53463
        },
        nativeLanguage: [1, 2],
        targetLanguage: [0, 1],
    },
    {
        nativeLanguage: [1, 2],
    }
]

const invalidTestCase = [
    {
        error: 'dob invalid',
        data: [
            { dob: '03-03-1993' },
            { dob: '23-03-2003' },
            { dob: '1990-13-01' },
            { dob: '1993-02-31' },
            { dob: 'fajfjuryh' },
            { dob: '' }
        ]
    },
    {
        error: 'gender invalid',
        data: [
            { gender: '' },
            { gender: -1 },
            { gender: 3 }
        ]
    },
    {
        error: 'language invalid',
        data: [
            { nativeLanguage: [0, 0] },
            { targetLanguage: [0, 0], nativeLanguage: [0, 1] },
            { targetLanguage: [0, 0] },
            { targetLanguage: [1, 1], nativeLanguage: [0, 1] },
            { targetLanguage: [1, 1], nativeLanguage: [] }
        ]
    },
    {
        error: 'address invalid',
        data: [
            { 
                address: {
                    detailAddress: 'test address',
                    latitude: 'wrong format',
                    longitude: 133.34353
                }
            },
            { 
                address: {
                    detailAddress: 'test address',
                    latitude: 33.34353
                }
            }

        ]
    }
]

describe('Test update user info validation: ', function() {
    describe('Valid vase test', function() {
        validTestCase.forEach(value => {
            it('should be valid', async function() {
                var val = await validators.updateUserInfoRequestValidator(value);
                expect(value).equal(val);
            });
        });
    });

    invalidTestCase.forEach(invalidCase => {
        describe('Test ' + invalidCase.error, function() {
            invalidCase.data.forEach(value => {
                it(JSON.stringify(value) + ' should be invalid', async function() {
                    // expect(validators.updateUserInfoRequestValidator(value)).to.eventually.throw(RequestError);
                    await expect(validators.updateUserInfoRequestValidator(value)).to.eventually.rejectedWith(RequestError);
                })
            })
        })
    })
});