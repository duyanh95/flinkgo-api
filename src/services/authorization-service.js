const jwtAuth = require('../helper/jwt-authentication');
const { AuthorizationError, AuthenticationError } = require('../custom-error');

// authenticate and populate req.locals.user with authenticated user
function authorizeRequest(req, res, next) {
    var header = req.header('Authorization');

    if (!header)
        throw new AuthenticationError('Request not authenticated');

    var headerParts = header.split(' ');
    if (headerParts[0] !== 'Bearer') 
        throw new AuthenticationError('Request not authenticated');

    return jwtAuth.verifyJWT(headerParts[1])
    .then(decoded => {
        req.locals = req.locals ? req.locals : {};
        req.locals.user = decoded.user;
        next();
    }).catch(next);
}

module.exports = {
    authorizeRequest
}