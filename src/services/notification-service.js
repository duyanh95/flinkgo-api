var notification = require('../lib/notification');
var UserModel = require('../models/user');
var NotificationModel = require('../models/notification')
const Logger = require('../helper/logger');

const SYSTEM_TOPIC = 'system';
const log = new Logger().getInstance();

async function getUserNotification(user, pagination) {
    return NotificationModel.find({ topic: 'uid' + user.id }).sort({ sentAt: 'desc' })
        .limit(Number(pagination.size)).skip(pagination.page * pagination.size).exec();
}

async function sendNotificationToUser(message, user) {
    var topic = 'uid' + user._id

    await notification.sendToTopic(topic, message);

    let noti = {
        data: message,
        type: "INVITATION",
        topic: topic,
        sentAt: new Date()
    }
    // message.topic = topic;
    return NotificationModel(noti).save();
}

function subscribeUserDevice(user, device) {
    notification.subscribeToken(device.token, SYSTEM_TOPIC);
}

async function saveUserDevice(user, device) {
    var existed = false;
    user.device.some((dev, idx) => {
        if (dev.id === device.id) {
            existed = true;
            user.device[idx] = device

            return true;            
        }
    })

    if (!existed)
        user.device.push(device);

    return UserModel.updateOne({ "_id": user.id }, {device: user.device}).then(rs => {
        subscribeUserDevice(user, device);
    });
}

module.exports = {
    saveUserDevice,
    sendNotificationToUser,
    getUserNotification
}