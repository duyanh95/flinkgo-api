const jwtAuth = require('../helper/jwt-authentication');
const UserModel = require('../models/user');
const encrypt = require('../helper/encryption');
var Promise = require('bluebird');

async function generateUserTokens(user, option) {
    if (user.authentication == undefined) 
        user.authentication = {};

    promises = [];

   
    if (option !== 'accessOnly') {
        promises.push(
            encrypt.genrateRandomBytes(24)
            .then(res => user.authentication.refreshToken = res)
        );
    }

    promises.push(
        jwtAuth.generateJWT(user)
        .then(res => user.authentication.accessToken = res)
    );
           
    return Promise.all(promises)
    .then(result => {
        return new UserModel(user).save();
    });
}

module.exports = { 
    generateUserTokens
};