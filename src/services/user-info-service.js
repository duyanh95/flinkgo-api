var UserModel = require('../models/user');
var converter = require('./schedule-converter');

async function createSchedule(user, data) {
    var schedule = converter.convertRequestToScheduleData(data.schedule)
    console.log('schedule', [schedule]);
    return UserModel.updateOne({ _id: user.id }, { schedule: schedule }).exec();
}

async function updateInfo(user, info) {
    return UserModel.updateOne({ _id: user.id }, info).exec();
}

async function getUser(user) {
    return UserModel.findById(user.id).exec();
}

async function getUsersByIds(ids) {
    return UserModel.findByIds(ids);
}

module.exports = {
    createSchedule,
    updateInfo,
    getUser,
    getUsersByIds
}