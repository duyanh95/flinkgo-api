var UserModel = require('../models/user');
const { DataConflictError } = require('../custom-error');
const encrypt = require('../helper/encryption');
const { generateUserTokens } = require('./token-service');
var Promise = require('bluebird')

async function registerUserAndGenerateTokens(user) {
    return registerUser(user)
    .then(user => generateUserTokens(user, ''));
}

async function registerUser(user) {
    var existed = checkUserExist(user);

    var updateAuthInfo = updateAuthenticationInfo(user);

    return Promise.join(existed, updateAuthInfo, function (exist, user) {
        return UserModel(user).save();
    });
}

async function checkUserExist(user) {
    return UserModel.findOne({ username: user.username })
    .then(user => {
        if (user != undefined || user != null)
            throw new DataConflictError('Username already existed');

        return user;
    });
}

function updateUserData(user, salt, password) {
    user.authentication = {
        salt: salt,
        password: password
    }

    return user;
}

function updateAuthenticationInfo(user) {
    return encrypt.genrateRandomBytes(16)
    .then(salt =>  {
        return encrypt.passwordEncrypt(user.password, salt)
        .then(encPass => {
            return updateUserData(user, salt, encPass);
        });
    })

}

module.exports = {
    registerUserAndGenerateTokens
}