const UserModel = require('../models/user');
const { AuthenticationError } = require('../custom-error');
const encrypt = require('../helper/encryption');
const { generateUserTokens } = require('./token-service');

async function refreshUserWithToken(token, body) {
    var user = await UserModel.findOne({ 'authentication.refreshToken': token });

    if (body.address) {
        user.address = body.address;
        await user.save()
    }

    return generateUserTokens(user, 'accessOnly')
       
}

async function authenticateLoginUser(body) {
    var user = await UserModel.findOne({ username: body.username });

    if (body.address) {
        user.address = body.address;
        await user.save()
    }

    return authenticateUser(user, body);
}

async function authenticateUser(user, body) {

    return checkPassword(user, body.password)
    .then(generateUserTokens(user, 'accessOnly'))
    .then(rs => {return user});
}

function checkPassword(user, password, callback) {
    if (!user)
        throw new AuthenticationError('Invalid username/password');

    return encrypt.passwordVerify(password, user.authentication.salt, user.authentication.password)
    .catch(err => {throw new AuthenticationError('Invalid username/password')});
}

module.exports = {
    refreshUserWithToken,
    authenticateLoginUser
}