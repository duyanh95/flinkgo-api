const MAX_TIME = 4;
const BIT_LENGTH = 32;

function convertRequestToScheduleData(req) {
    var data = 0x00000000;

    req.forEach(day => {

        var currentDay = day.day;
        day.time.forEach(time => {
            
            // find corresponding bit position
            var shift = currentDay * MAX_TIME + time;

            // set corresponding bit for each time
            data = data | (1 << shift);
        });
    });
    
    return data;
}

function convertScheduleDataToResponse(data) {
    var schedule = [];

    for (var day = 0; day < BIT_LENGTH / 4; day++) {
        var dayBitLoc = day * MAX_TIME - 1

        var dayMask = (data >> (day * MAX_TIME)) & 0xf;

        // if current day has data create new day entry
        if (dayMask) {
            var currentDay = {
                day: day,
                time: []
            };

            for (var time = 0; time < MAX_TIME; time++) {
                // if time bit is 1 add new time entry
                if (dayMask & (1 << time)) {
                    currentDay.time.push(time);
                }
            }

            schedule.push(currentDay);
        }
    }

    return schedule;
}

module.exports = {
    convertRequestToScheduleData,
    convertScheduleDataToResponse
}