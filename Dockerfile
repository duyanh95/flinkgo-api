FROM node:8.16-alpine

WORKDIR ~/

COPY ./src ./src
COPY package*.json ./

RUN npm install --only=production
EXPOSE ${PORT}
CMD [ "node", "./src/index.js"]